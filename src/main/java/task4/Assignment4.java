package task4;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Assignment4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

Scanner sc = new Scanner(System.in);
		
		System.out.println("Type your String");
		String inputString = sc.nextLine();
		
		String lowerCase = inputString.toLowerCase();
		
		Set<String> setObject = new HashSet<String>() ;
		
		for(int i = 0;i<lowerCase.length();i++) {
			setObject.add(String.valueOf(lowerCase.charAt(i)));
		}
		
		if(setObject.size() == lowerCase.length())
			System.out.println("Distinct letters");
		
		else
			System.out.println("Not Distinct letters");
			
	}

}
