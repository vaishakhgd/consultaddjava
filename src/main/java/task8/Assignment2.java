package task8;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Assignment2 {
	
	static int count = 0;
	
	public static void foo() {
		count++;
	}
	
	public static void main(String[] args) {
		
		String str = "abc one two three plemty ";
		
		
		
		Stream.of(str.split(" "))
	      .forEach (elem ->  Assignment2.foo());
		
		System.out.println(count);
	      
	}

}
