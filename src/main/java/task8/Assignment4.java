package task8;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class Assignment4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
List<Integer>  streamList = Arrays.asList(1,2,3,4,0,10,6);
		
		Optional<Integer> max = streamList.stream().reduce((x,y) -> x>y?x:y);
		
		System.out.println(max);
		
		Optional<Integer> sum = streamList.stream().reduce((x,y) -> x+y);

		
		
		System.out.println(sum.get()/streamList.size());
	}

}
