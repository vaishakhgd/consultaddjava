package task8;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Assignment6 {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		 File f = new File("p.txt");
		f.createNewFile();
		
		List<Integer>  streamList = Arrays.asList(1,2,3,4,0,10,6);
		
	String q = 	streamList.stream().map( i -> String.valueOf(i*i)).collect(Collectors.joining(","));;
	
	System.out.println(q);
	//q.toString();
	
	BufferedWriter writer = new BufferedWriter(new FileWriter("p.txt", true));
    writer.append(' ');
    writer.append(q);
    
    writer.close();
	
	

	}

}
