package task8;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Assignment1 {
	
	static Map<String,Integer> mapObj = new HashMap<>();
	
	private static boolean isNotDone=false;
	
	public static  void function(String s) {
		
		//System.out.println(mapObj);
		
		if(isNotDone==false) {
			
			if(mapObj.containsKey(s)) {
				
				isNotDone = true;
				System.out.println(s);
			}
			
			else {
				
				//
				mapObj.put(s, 1);
				
			}
			
		
		
		}
		
		
		
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		List<String>  streamList = Arrays.asList("a","b","c","c","a","b");
		
		
		   streamList.stream().forEach(f -> Assignment1.function(f) );
		
		
					
		}
	
		
		
	
	}


