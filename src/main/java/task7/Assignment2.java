package task7;

import java.util.HashMap;
import java.util.Map;

public class Assignment2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s = "aabbcddeeff";
		
		StringBuilder sb = new StringBuilder(s);
		
		Map<String,Integer> mapObj = new HashMap<>();
		
		String firstChar = "";
		boolean isFirstCharSet = false;
		
		for(int i=0;i<s.length();i++) {
			
			if(mapObj.containsKey(String.valueOf(s.charAt(i)))) {
				
				mapObj.remove(String.valueOf(s.charAt(i)));
				
				
			}else {
				
				mapObj.put(String.valueOf(s.charAt(i)), 1);
				
				if(!isFirstCharSet) {
					firstChar = String.valueOf(s.charAt(i));
					isFirstCharSet = true;
				}
			}
			
			
		}
		
	//	System.out.println(firstChar);
		
		for(String s1 : mapObj.keySet()) {
			System.out.println(s1);
			break;
		}
		
		
		

	}

}
