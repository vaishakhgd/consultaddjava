package task7;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class Assignment11 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Map<Integer,Integer> unsortedMap = new HashMap<>();
		
		unsortedMap.put(1, 100);
		unsortedMap.put(10, 101);
		unsortedMap.put(9, 50);
		unsortedMap.put(1000, 40);

		Map<Object, Object> sortedMap = 
			     unsortedMap.entrySet().stream()
			    .sorted(Entry.comparingByValue())
			    .collect(Collectors.toMap(Entry::getKey, Entry::getValue,
			                              (e1, e2) -> e1, LinkedHashMap::new));
		
		System.out.println("sort by value"+sortedMap);
		
		Map<Object, Object> sortedMapByKey = 
			     unsortedMap.entrySet().stream()
			    .sorted(Entry.comparingByKey())
			    .collect(Collectors.toMap(Entry::getKey, Entry::getValue,
			                              (e1, e2) -> e1, LinkedHashMap::new));
	
	
		System.out.println("sort by value"+sortedMapByKey);
	
	}

}
