package task5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Assignment1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr1 = {1,2,3,4,5,6,1};
		
		List<Integer> l1 = new ArrayList<>();
		
		for(int a : arr1) {
			l1.add(a);
		}
		
		Set<Integer> s1 = new HashSet<>();
		
		s1.addAll(l1);
		
		System.out.println(s1);

	}

}
