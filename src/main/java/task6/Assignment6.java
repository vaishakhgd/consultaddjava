package task6;

class A{
	
	public A() {
		System.out.println("I love programming languages");
	}
	
	public A(String str) {
		System.out.println("I love "+ str);
		
	}
	
	
}

public class Assignment6 {
	
	public static void main(String[] args) {
		
		A ob1 = new A();
		
		A ob2 = new A("joe");
		
	}

}
