package task6;

class Details{
	
	String name,year,address;
	
	Details(String name,String year,String address){
		
		this.name=name;
		this.year=year;
		this.address=address;
	}
	
	@Override
	public String toString() {
		return this.name +","+this.year+","+this.address;
	}
}

public class Assignment2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(new Details("Robert ","1994","64C- Walls Streat"));
		
		System.out.println(new Details("Sam ","2000","68D- Walls Streat"));

		
		System.out.println(new Details("john ","1999","26B- Walls Streat"));


	}

}
