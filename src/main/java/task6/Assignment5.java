package task6;


class AddAmount{
	static int amount = 50;
	
	public static  int noOfTransactions = 0;
	
	void printTransactionCount() {
		
		System.out.println(this.noOfTransactions);
	}
	
	public AddAmount() {
		this.noOfTransactions++;
		
	}
	
public AddAmount(int amount) {
		
		this.amount+=amount;
		this.noOfTransactions++;
	}
	
	
} 

public class Assignment5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		AddAmount addAmount = new AddAmount();
		
		addAmount.printTransactionCount();
		
		AddAmount addAmount2 = new AddAmount(50);
		
		addAmount.printTransactionCount();
		
		
AddAmount addAmount3 = new AddAmount(150);
		
		addAmount.printTransactionCount();
		
		System.out.println(AddAmount.amount);
		
		
		
		
		

	}

}
