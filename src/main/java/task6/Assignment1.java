package task6;

class Complex{
	
	double real;
	double imaginary;
	
	Complex(double real,double imaginary){
		this.real = real;
		this.imaginary = imaginary;
	}
	
	Complex(){
		
		
	}
	
	@Override
	public String toString(){
		return String.valueOf(this.real) + "+" + "i" + String.valueOf(this.imaginary);
	}
	
	public Complex add(Complex a,Complex b) {
		
		return new Complex(a.real+b.real,a.imaginary+b.imaginary);
	}
	
public Complex subtract(Complex a,Complex b) {
	return new Complex(a.real-b.real,a.imaginary-b.imaginary);
	}

public Complex multiply(Complex a,Complex b) {
	return new Complex(a.real*b.real- (a.imaginary*b.imaginary),a.real*b.imaginary+a.imaginary*b.real);
	}
	
	
}

public class Assignment1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Complex c1 = new Complex(2,3);
		Complex c2 = new Complex(3,5);
		
		Complex complex = new Complex();
		
		System.out.println(complex.add(c2, c1).toString());
	
		System.out.println(complex.multiply(c2, c1).toString());
		System.out.println(complex.subtract(c2, c1).toString());

	}

}
